package com.kture.spring;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class JdbcTestConfig {

    //db for test
    @Bean(name = "testDataSource")
    @Primary
    public DataSource testDataSource() {
        return DataSourceBuilder.create()
                       .username("postgres")
                       .password("1123581321")
                       .url("jdbc:postgresql://localhost:5432/spring_lab_db_test")
                       .driverClassName("org.postgresql.Driver")
                       .build();
    }

    @Bean
    public JdbcTemplate jdbcTemplate(){
        return new JdbcTemplate(testDataSource());
    }
}
