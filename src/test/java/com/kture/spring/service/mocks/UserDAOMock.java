package com.kture.spring.service.mocks;

import com.kture.spring.dao.ItemNotFoundException;
import com.kture.spring.dao.UserDAO;
import com.kture.spring.entity.User;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Profile("dao_mock")
@Component
public class UserDAOMock implements UserDAO {
    @Override
    public Optional<User> getByEmail(String email) {
        return Optional.of(new User("user", email, "000"));
    }

    @Override
    public List<User> getByName(String name) {
        return Arrays.asList(new User("user", "user@user.com", "000"),
                                    new User("user1", "user1@user1.com", "111"));
    }

    @Override
    public User createItem(User item) {
        item.setId(1);
        return item;
    }

    @Override
    public Optional<User> getItemById(long id) {
        if (id == 10) {
            return Optional.empty();
        }

        User user = new User("user", "user@user.com", "000");
        user.setId(id);
        return Optional.of(user);
    }

    @Override
    public User update(User item) throws ItemNotFoundException {
        if (item.getId() == 1) {
            return item;
        } else {
            throw new ItemNotFoundException();
        }
    }

    @Override
    public boolean delete(long id) {
        return true;
    }
}
