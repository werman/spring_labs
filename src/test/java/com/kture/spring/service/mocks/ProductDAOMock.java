package com.kture.spring.service.mocks;

import com.kture.spring.dao.ItemNotFoundException;
import com.kture.spring.dao.ProductDAO;
import com.kture.spring.entity.Product;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Profile("dao_mock")
@Component
public class ProductDAOMock implements ProductDAO {

    @Override
    public List<Product> getByUserId(long id) {
        return Arrays.asList(new Product("Nice product", "Mmmmphhhhh", 10, id),
                                    new Product("Bad product", "very bad", 2, id));
    }

    @Override
    public List<Product> getByTitle(String title) {
        return Arrays.asList(new Product(title, "Mmmmphhhhh", 10, 1),
                                    new Product(title, "very bad", 2, 1));
    }

    @Override
    public Product createItem(Product item) {
        item.setId(10);
        return item;
    }

    @Override
    public Optional<Product> getItemById(long id) {
        Product product = new Product("Nice product", "Mmmmphhhhh", 10, 1);
        product.setId(id);
        return Optional.of(product);
    }

    @Override
    public Product update(Product item) throws ItemNotFoundException {
        if (item.getId() == 1) {
            return item;
        } else {
            throw new ItemNotFoundException();
        }
    }

    @Override
    public boolean delete(long id) {
        return true;
    }
}
