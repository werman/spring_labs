package com.kture.spring.service;

import com.kture.spring.MainTest;
import com.kture.spring.dao.ProductDAO;
import com.kture.spring.dao.UserDAO;
import com.kture.spring.entity.Product;
import com.kture.spring.service.exceptions.ProductNotFoundException;
import com.kture.spring.service.exceptions.UserNotFoundException;
import com.kture.spring.service.mocks.ProductDAOMock;
import com.kture.spring.service.mocks.UserDAOMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ProductServiceImplTest {

    @Configuration
    static class  ProductServiceImplTestContextConfiguration {

        @Bean
        public ProductDAO productDAO() {
            return new ProductDAOMock();
        }

        @Bean
        public UserDAO userDAO() {
            return new UserDAOMock();
        }

        @Bean ProductService productService() {
            return new ProductServiceImpl();
        }
    }

    @Autowired
    private ProductService productService;

    @Test
    public void testCreateProduct() {
        Product product = new Product("test", "test", 10, 1);
        product = productService.createProduct(product);
        assertEquals(10l, product.getId());
    }

    @Test
    public void testGetProductById() {
        Optional<Product> product = productService.getProductById(1);
        assertEquals("Nice product", product.get().getTitle());
    }

    @Test
    public void testUpdateProduct() {
        Product product = new Product("update", "update", 10, 1);
        product.setId(1);
        product = productService.updateProduct(product);
        assertEquals("update", product.getTitle());
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateProductUserNotFound() {
        Product product = new Product("update", "update", 10, 10);
        product.setId(1);
        product = productService.updateProduct(product);
    }

    @Test(expected = ProductNotFoundException.class)
    public void testUpdateProductProductNotFound() {
        Product product = new Product("update", "update", 10, 1);
        product.setId(2);
        product = productService.updateProduct(product);
    }

    @Test
    public void testDelete() {
        boolean success = productService.delete(1);
        assertTrue(success);
    }

    @Test
    public void testGetByUserId() {
        List<Product> products = productService.getByUserId(10);
        assertEquals(2, products.size());
    }

    @Test
    public void testGetByTitle() {
        List<Product> products = productService.getByTitle("test");
        assertEquals(2, products.size());
    }
}
