package com.kture.spring.controller.mocks;

import com.kture.spring.entity.Product;
import com.kture.spring.entity.User;
import com.kture.spring.facade.MarketFacade;
import com.kture.spring.service.exceptions.NotEnoughFundsOnBalance;

import java.util.Arrays;
import java.util.List;

public class MarketFacadeMock implements MarketFacade {
    @Override
    public User createUser(User user) {
        return null;
    }

    @Override
    public User updateUser(User user) {
        return null;
    }

    @Override
    public User getUserById(long id) {
        return null;
    }

    @Override
    public User getUserByEmail(String email) {
        return null;
    }

    @Override
    public List<User> getUsersByName(String name) {
        return Arrays.asList(new User(name, "john@gmail.com", "00-00"), new User(name, "bob@mail.ru", "11-11"));
    }

    @Override
    public boolean deleteUser(long id) {
        return false;
    }

    @Override
    public Product createProduct(Product product) {
        return null;
    }

    @Override
    public Product updateProduct(Product product) {
        return null;
    }

    @Override
    public Product getProductById(long id) {
        return null;
    }

    @Override
    public List<Product> getProductsByTitle(String title) {
        return null;
    }

    @Override
    public List<Product> getProductsByUserId(long id) {
        return null;
    }

    @Override
    public boolean deleteProduct(long id) {
        return false;
    }

    @Override
    public void buyProduct(Product product, User buyer) throws NotEnoughFundsOnBalance {

    }
}
