package com.kture.spring.dao;

import com.kture.spring.MainTest;
import com.kture.spring.entity.Product;
import com.kture.spring.storage.MarketStorage;
import com.kture.spring.storage.Storage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class StorageProductDAOTest {

    @Configuration
    public static class StorageProductDAOTestContextConfiguration {
        @Bean
        public ProductDAO productDAO() {
            return new StorageProductDAO();
        }

        @Bean
        public Storage storage() {
            return new MarketStorage();
        }
    }

    @Autowired
    private Storage storage;

    @Autowired
    private ProductDAO productDAO;


    @Before
    public void setUp() {
        Map<Long, Product> products = new HashMap<>();
        Product product = new Product("Nice product", "Mmmmphhhhh", 10, 1);
        product.setId(1l);
        products.put(product.getId(), product);

        product = new Product("Bad product", "very bad", 2, 1);
        product.setId(2l);
        products.put(product.getId(), product);

        storage.getProducts().clear();
        storage.getProducts().putAll(products);
    }

    @Test
    public void testGetByUserId() {
        List<Product> product = productDAO.getByUserId(1);
        assertEquals(2, product.size());
    }

    @Test
    public void testGetByTitle() {
        List<Product> products = productDAO.getByTitle("Bad product");
        assertEquals(1, products.size());
        assertEquals(2l, products.get(0).getId());
    }

    @Test
    public void testCreateItem() {
        Product product = new Product("product", "description", 9, 10);
        productDAO.createItem(product);

        Optional<Product> queriedProduct = productDAO.getItemById(product.getId());
        assertEquals(product.getId(), queriedProduct.get().getId());
        assertEquals("description", queriedProduct.get().getDescription());
    }

    @Test
    public void testGetItemById() {
        Optional<Product> product = productDAO.getItemById(1);
        assertEquals(1, product.get().getId());
        assertEquals("Nice product", product.get().getTitle());
    }

    @Test
    public void testUpdate() throws ItemNotFoundException {
        Product product = new Product("product", "description", 9, 10);
        product.setId(1l);

        productDAO.update(product);
        Optional<Product> queriedProduct = productDAO.getItemById(product.getId());
        assertEquals(1l, queriedProduct.get().getId());
        assertEquals("product", queriedProduct.get().getTitle());
    }

    @Test(expected = ItemNotFoundException.class)
    public void testUpdateThrow() throws ItemNotFoundException {
        Product product = new Product("product", "description", 9, 10);
        product.setId(99l);

        productDAO.update(product);
    }

    @Test
    public void testDelete() {
        boolean success = productDAO.delete(1l);

        Optional<Product> product = productDAO.getItemById(1);
        assertTrue(!product.isPresent());
        assertEquals(true, success);
    }
}
