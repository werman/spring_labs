package com.kture.spring.dao;

import com.kture.spring.JdbcTestConfig;
import com.kture.spring.entity.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JdbcTestConfig.class, DBProductDAOTest.StorageProductDAOTestContextConfiguration.class})
public class DBProductDAOTest {

    @Configuration
    public static class StorageProductDAOTestContextConfiguration {
        @Bean
        public ProductDAO productDAO() {
            return new DBProductDAO();
        }
    }

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/populate_products.sql")));
        LineNumberReader fileReader = new LineNumberReader(in);
        String sql = ScriptUtils.readScript(fileReader, "//", ";");
        jdbcTemplate.execute(sql);
    }

    @Test
    public void testGetByUserId() {
        List<Product> product = productDAO.getByUserId(1);
        assertEquals(2, product.size());
    }

    @Test
    public void testGetByTitle() {
        List<Product> products = productDAO.getByTitle("Bad product");
        assertEquals(1, products.size());
        assertEquals(2l, products.get(0).getId());
    }

    @Test
    public void testCreateItem() {
        Product product = new Product("product", "description", 9, 10);
        productDAO.createItem(product);

        Optional<Product> queriedProduct = productDAO.getItemById(product.getId());
        assertEquals(product.getId(), queriedProduct.get().getId());
        assertEquals("description", queriedProduct.get().getDescription());
    }

    @Test
    public void testGetItemById() {
        Optional<Product> product = productDAO.getItemById(1);
        assertEquals(1, product.get().getId());
        assertEquals("Nice product", product.get().getTitle());
    }

    @Test
    public void testUpdate() throws ItemNotFoundException {
        Product product = new Product("product", "description", 9, 10);
        product.setId(1l);

        productDAO.update(product);
        Optional<Product> queriedProduct = productDAO.getItemById(product.getId());
        assertEquals(1l, queriedProduct.get().getId());
        assertEquals("product", queriedProduct.get().getTitle());
    }

    @Test(expected = ItemNotFoundException.class)
    public void testUpdateThrow() throws ItemNotFoundException {
        Product product = new Product("product", "description", 9, 10);
        product.setId(99l);

        productDAO.update(product);
    }

    @Test
    public void testDelete() {
        boolean success = productDAO.delete(1l);

        Optional<Product> product = productDAO.getItemById(1);
        assertTrue(!product.isPresent());
        assertEquals(true, success);
    }

}
