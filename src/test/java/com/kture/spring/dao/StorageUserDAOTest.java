package com.kture.spring.dao;

import com.kture.spring.MainTest;
import com.kture.spring.entity.User;
import com.kture.spring.storage.MarketStorage;
import com.kture.spring.storage.Storage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class StorageUserDAOTest {

    @Configuration
    static class StorageUserDAOTestContextConfiguration {
        @Bean
        public Storage storage() {
            return new MarketStorage();
        }

        @Bean
        public UserDAO userDAO() {
            return new StorageUserDAO();
        }
    }

    @Autowired
    private Storage storage;

    @Autowired
    private UserDAO userDAO;


    @Before
    public void setUp() {
        Map<Long, User> users = new HashMap<>();
        User user = new User("John", "john@gmail.com", "000-000-000");
        user.setId(1l);
        users.put(user.getId(), user);
        storage.getUsers().putAll(users);

        user = new User("John", "bob@gmail.com", "100-000-001");
        user.setId(5l);
        users.put(user.getId(), user);

        storage.getUsers().clear();
        storage.getUsers().putAll(users);
    }

    @Test
    public void testGetItemById() {
        Optional<User> user = userDAO.getItemById(1);
        assertEquals(1, user.get().getId());
        assertEquals("John", user.get().getName());
    }

    @Test
    public void testCreateItem() {
        User user = new User("007", "007@mail.com", "007-007-007");
        userDAO.createItem(user);

        Optional<User> queriedUser = userDAO.getItemById(user.getId());
        assertEquals(user.getId(), queriedUser.get().getId());
        assertEquals("007", queriedUser.get().getName());
    }

    @Test
    public void testUpdate() throws ItemNotFoundException {
        User user = new User("008", "008@mail.com", "008-008-008");
        user.setId(1l);

        userDAO.update(user);
        Optional<User> queriedUser = userDAO.getItemById(user.getId());
        assertEquals(1l, queriedUser.get().getId());
        assertEquals("008", queriedUser.get().getName());
    }


    @Test(expected = ItemNotFoundException.class)
    public void testUpdateThrow() throws ItemNotFoundException {
        User user = new User("008", "008@mail.com", "008-008-008");
        user.setId(2l);

        userDAO.update(user);
    }

    @Test
    public void testDelete() {
        boolean success = userDAO.delete(1l);

        Optional<User> user = userDAO.getItemById(1l);
        assertTrue(!user.isPresent());
        assertEquals(true, success);
    }

    @Test
    public void testGetByEmail() {
        Optional<User> user = userDAO.getByEmail("john@gmail.com");
        assertEquals(1, user.get().getId());
        assertEquals("John", user.get().getName());
    }

    @Test
    public void testGetByName() {
        List<User> users = userDAO.getByName("John");
        assertEquals(2, users.size());
    }
}
