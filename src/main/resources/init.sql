DROP TABLE IF EXISTS Products;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS UserAccounts;

CREATE TABLE Products (
  id          INTEGER PRIMARY KEY,
  title       VARCHAR(255),
  description VARCHAR(255),
  price       INTEGER,
  userId      INTEGER
);

CREATE TABLE Users (
  id          INTEGER PRIMARY KEY,
  name        VARCHAR(255),
  email       VARCHAR(255),
  phoneNumber VARCHAR(32)
);

CREATE TABLE UserAccounts (
  userId  INTEGER PRIMARY KEY,
  balance INTEGER
);