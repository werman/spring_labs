package com.kture.spring.dao;

import com.kture.spring.storage.Storage;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractDAO {

    @Autowired
    private Storage storage;

    private long lastId = 0;

    public Storage getStorage() {
        return storage;
    }

    long generateId() {
        return lastId++;
    }
}
