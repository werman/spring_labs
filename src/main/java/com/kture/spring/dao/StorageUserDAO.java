package com.kture.spring.dao;

import com.kture.spring.entity.User;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Profile({"dao_mem", "dao_db"})
@Component
public class StorageUserDAO extends AbstractDAO implements UserDAO {

    public Optional<User> getByEmail(String email) {
        return getStorage()
                       .getUsers().values().stream()
                       .filter(u -> u.getEmail().equals(email))
                       .findFirst();
    }

    public List<User> getByName(String name) {
        return getStorage()
                       .getUsers().values().stream()
                       .filter(u -> u.getName().equals(name))
                       .collect(Collectors.toList());
    }

    public User createItem(User item) {
        item.setId(generateId());
        getStorage().getUsers().put(item.getId(), item);
        return item;
    }

    public Optional<User> getItemById(long id) {
        return getStorage()
                       .getUsers().values().stream()
                       .filter(u -> u.getId() == id)
                       .findFirst();
    }

    public User update(User item) throws ItemNotFoundException {
        if (!getStorage().getUsers().containsKey(item.getId())) {
            throw new ItemNotFoundException();
        }

        getStorage().getUsers().put( item.getId(), item);
        return item;
    }

    public boolean delete(long id) {
        return getStorage().getUsers().remove( id ) != null;
    }
}
