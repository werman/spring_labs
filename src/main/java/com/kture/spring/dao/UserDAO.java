package com.kture.spring.dao;

import com.kture.spring.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserDAO extends DAO<User> {

    Optional<User> getByEmail(String email);

    List<User> getByName(String name);
}
