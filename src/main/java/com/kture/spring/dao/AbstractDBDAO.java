package com.kture.spring.dao;

public abstract class AbstractDBDAO {

    private long lastId = 0;

    long generateId() {
        return lastId++;
    }
}
