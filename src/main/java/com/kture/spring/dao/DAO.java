package com.kture.spring.dao;

import java.util.Optional;

interface DAO<T> {

    T createItem(T item);

    Optional<T> getItemById(long id);

    T update(T item) throws ItemNotFoundException;

    boolean delete(long id);
}
