package com.kture.spring.dao.mappers;

import com.kture.spring.entity.UserAccount;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserAccountMapper implements RowMapper<UserAccount> {
    @Override
    public UserAccount mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        UserAccount account = new UserAccount();
        account.setUserId(resultSet.getLong("userId"));
        account.setBalance(resultSet.getLong("balance"));
        return account;
    }
}
