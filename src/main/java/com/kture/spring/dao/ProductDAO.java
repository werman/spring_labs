package com.kture.spring.dao;

import com.kture.spring.entity.Product;

import java.util.List;

public interface ProductDAO extends DAO<Product> {

    List<Product> getByUserId(long id);

    List<Product> getByTitle(String title);
}
