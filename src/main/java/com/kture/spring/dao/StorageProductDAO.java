package com.kture.spring.dao;

import com.kture.spring.entity.Product;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Profile("dao_mem")
@Component
public class StorageProductDAO extends AbstractDAO implements ProductDAO {
    public List<Product> getByUserId(long id) {
        return getStorage()
                       .getProducts().values().stream()
                       .filter(u -> u.getUserId() == id)
                       .collect(Collectors.toList());
    }

    public List<Product> getByTitle(String title) {
        return getStorage()
                       .getProducts().values().stream()
                       .filter(u -> Objects.equals(u.getTitle(), title))
                       .collect(Collectors.toList());
    }

    public Product createItem(Product item) {
        item.setId(generateId());
        getStorage().getProducts().put(item.getId(), item);
        return item;
    }

    public Optional<Product> getItemById(long id) {
        return getStorage()
                       .getProducts().values().stream()
                       .filter(u -> u.getId() == id)
                       .findFirst();
    }

    public Product update(Product item) throws ItemNotFoundException {
        if (!getStorage().getProducts().containsKey(item.getId())) {
            throw new ItemNotFoundException();
        }

        getStorage().getProducts().put(item.getId(), item);
        return item;
    }

    public boolean delete(long id) {
        return getStorage().getProducts().remove(id) != null;
    }
}
