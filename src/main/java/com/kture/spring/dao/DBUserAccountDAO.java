package com.kture.spring.dao;

import com.kture.spring.dao.mappers.UserAccountMapper;
import com.kture.spring.entity.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Profile("dao_db")
@Component
public class DBUserAccountDAO extends AbstractDBDAO implements UserAccountDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private UserAccountMapper userAccountMapper = new UserAccountMapper();

    @Override
    public UserAccount createItem(UserAccount item) {
        String SQL = "INSERT INTO UserAccounts (userId, balance) VALUES (?, ?)";
        jdbcTemplate.update(SQL, item.getUserId(), item.getBalance());
        return item;
    }

    @Override
    public Optional<UserAccount> getItemById(long id) {
        String SQL = "SELECT * FROM UserAccounts WHERE userId = ?";
        UserAccount userAccount;
        try {
            userAccount = jdbcTemplate.queryForObject(SQL, new Object[]{id}, userAccountMapper);
        } catch (EmptyResultDataAccessException ex) {
            userAccount = null;
        }

        return Optional.ofNullable(userAccount);
    }

    @Override
    public UserAccount update(UserAccount item) throws ItemNotFoundException {
        String SQL = "UPDATE UserAccounts SET balance = ? WHERE userId = ?";
        int result = jdbcTemplate.update(SQL, item.getBalance(), item.getUserId());
        if (result == 0) {
            throw new ItemNotFoundException();
        }

        return item;
    }

    @Override
    public boolean delete(long id) {
        String SQL = "DELETE FROM UserAccounts WHERE userId = ?";
        int result = jdbcTemplate.update(SQL, id);
        return result == 1;
    }
}
