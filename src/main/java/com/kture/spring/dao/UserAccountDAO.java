package com.kture.spring.dao;

import com.kture.spring.entity.UserAccount;

public interface UserAccountDAO extends DAO<UserAccount>  {

}
