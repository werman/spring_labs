package com.kture.spring.dao;

import com.kture.spring.dao.mappers.ProductMapper;
import com.kture.spring.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Profile("dao_db")
@Component
public class DBProductDAO extends AbstractDBDAO implements ProductDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private ProductMapper productMapper = new ProductMapper();

    public List<Product> getByUserId(long id) {
        String SQL = "SELECT * FROM Products WHERE userId = ?";
        return jdbcTemplate.query(SQL, new Object[]{id}, productMapper);
    }

    public List<Product> getByTitle(String title) {
        String SQL = "SELECT * FROM Products WHERE title = ?";
        return jdbcTemplate.query(SQL, new Object[]{title}, productMapper);
    }

    public Product createItem(Product item) {
        item.setId(generateId());
        String SQL = "INSERT INTO Products (id, title, description, price, userId) VALUES (?, ?, ?, ?, ?)";
        jdbcTemplate.update(SQL, item.getId(), item.getTitle(), item.getDescription(), item.getPrice(), item.getUserId());
        return item;
    }

    public Optional<Product> getItemById(long id) {
        String SQL = "SELECT * FROM Products WHERE id = ?";
        Product product;
        try {
            product = jdbcTemplate.queryForObject(SQL, new Object[]{id}, productMapper);
        } catch (EmptyResultDataAccessException ex) {
            product = null;
        }

        return Optional.ofNullable(product);
    }

    public Product update(Product item) throws ItemNotFoundException {
        String SQL = "UPDATE Products SET title = ?, description = ?, price = ?, userId = ? WHERE id = ?";
        int result = jdbcTemplate.update(SQL, item.getTitle(), item.getDescription(), item.getPrice(), item.getUserId(), item.getId());
        if (result == 0) {
            throw new ItemNotFoundException();
        }

        return item;
    }

    public boolean delete(long id) {
        String SQL = "DELETE FROM Products WHERE id = ?";
        int result = jdbcTemplate.update(SQL, id);
        return result == 1;
    }
}
