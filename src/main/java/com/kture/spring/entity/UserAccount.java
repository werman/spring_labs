package com.kture.spring.entity;

public class UserAccount {
    private long userId;
    private long balance;

    public UserAccount() {
    }

    public UserAccount(long userId, long balance) {
        this.userId = userId;
        this.balance = balance;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
}
