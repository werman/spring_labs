package com.kture.spring.storage;

import com.kture.spring.entity.Product;
import com.kture.spring.entity.User;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class MarketStorage implements Storage {

//    @Value("${storage.init.file}")
//    private String initFile;

    private Map<Long, Product> products = new HashMap<>();
    private Map<Long, User> users = new HashMap<>();

    public Map<Long, User> getUsers() {
        return users;
    }

    public Map<Long, Product> getProducts() {
        return products;
    }

    @Override
    public void clearAll() {
        products.clear();
        users.clear();
    }

    @PostConstruct
    public void init() {

    }
}
