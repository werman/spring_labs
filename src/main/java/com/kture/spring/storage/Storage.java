package com.kture.spring.storage;

import com.kture.spring.entity.Product;
import com.kture.spring.entity.User;

import java.util.Map;

public interface Storage {

    Map<Long, User> getUsers();
    Map<Long, Product> getProducts();
    void clearAll();
}
