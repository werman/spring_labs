package com.kture.spring.service;


import com.kture.spring.dao.ItemNotFoundException;
import com.kture.spring.dao.ProductDAO;
import com.kture.spring.dao.UserDAO;
import com.kture.spring.entity.Product;
import com.kture.spring.entity.User;
import com.kture.spring.service.exceptions.ProductNotFoundException;
import com.kture.spring.service.exceptions.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl extends AbstractService implements ProductService {

    private static final Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    private ProductDAO productDAO;

    private UserDAO userDAO;

    @Override
    public Product createProduct(Product product) {
        Optional<User> productUser = userDAO.getItemById(product.getUserId());
        if (!productUser.isPresent()) {
            String errorMsg = String.format("Create product %d failed because no user with id %d doesn't exist.", product.getId(), product.getUserId());
            logger.warn(errorMsg);
            throw new UserNotFoundException(errorMsg);
        }

        Product result = productDAO.createItem(product);
        logger.info("Create product with id %d", result.getId());
        return result;
    }

    @Override
    public Optional<Product> getProductById(long id) {
        return productDAO.getItemById(id);
    }

    @Override
    public Product updateProduct(Product product) {
        Optional<User> productUser = userDAO.getItemById(product.getUserId());
        if (!productUser.isPresent()) {
            String errorMsg = String.format("Update product %d failed because no user with id %d doesn't exist.", product.getId(), product.getUserId());
            logger.warn(errorMsg);
            throw new UserNotFoundException(errorMsg);
        }

        logger.info("Update product with id %d", product.getId());
        try {
            return productDAO.update(product);
        } catch (ItemNotFoundException e) {
            String errorMsg = String.format("Update product failed because product with id %d doesn't exist.", product.getId());
            logger.warn(errorMsg, e);
            throw new ProductNotFoundException(errorMsg, e);
        }
    }

    @Override
    public boolean delete(long id) {
        logger.info("Delete product with id %d", id);
        return productDAO.delete(id);
    }

    @Override
    public List<Product> getByUserId(long id) {
        return productDAO.getByUserId(id);
    }

    @Override
    public List<Product> getByTitle(String title) {
        return productDAO.getByTitle(title);
    }

    @Autowired
    public void setProductDAO(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
}
