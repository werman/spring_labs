package com.kture.spring.service.exceptions;

public class NotEnoughFundsOnBalance extends Exception {
    public NotEnoughFundsOnBalance() {
        super();
    }

    public NotEnoughFundsOnBalance(String message) {
        super(message);
    }

    public NotEnoughFundsOnBalance(String message, Throwable cause) {
        super(message, cause);
    }
}
