package com.kture.spring.service.exceptions;

public class UserAccountNotFound extends RuntimeException {
    public UserAccountNotFound() {
        super();
    }
    public UserAccountNotFound(String message) {
        super(message);
    }
    public UserAccountNotFound(String message, Throwable cause) {
        super(message, cause);
    }
}
