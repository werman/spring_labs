package com.kture.spring.service;

import com.kture.spring.dao.ItemNotFoundException;
import com.kture.spring.dao.UserDAO;
import com.kture.spring.entity.User;
import com.kture.spring.service.exceptions.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl extends AbstractService implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private UserDAO userDAO;

    @Override
    public User createUser(User user) {
        User result = userDAO.createItem(user);
        logger.info("Create user with id %d", result.getId());
        return result;
    }

    @Override
    public Optional<User> getUserById(long id) {
        return userDAO.getItemById(id);
    }

    @Override
    public User updateUser(User user) {
        logger.info("Update user with id %d", user.getId());
        try {
            return userDAO.update(user);
        } catch (ItemNotFoundException e) {
            String errorMsg = String.format("Update user failed because user with id %d doesn't exist.", user.getId());
            logger.warn(errorMsg, e);
            throw new UserNotFoundException(errorMsg, e);
        }
    }

    @Override
    public boolean delete(long id) {
        logger.info("Delete user with id %d", id);
        return userDAO.delete(id);
    }

    @Override
    public Optional<User> getByEmail(String email) {
        return userDAO.getByEmail(email);
    }

    @Override
    public List<User> getByName(String name) {
        return userDAO.getByName(name);
    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
}
