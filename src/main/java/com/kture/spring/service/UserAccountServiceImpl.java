package com.kture.spring.service;


import com.kture.spring.dao.ItemNotFoundException;
import com.kture.spring.dao.UserAccountDAO;
import com.kture.spring.entity.UserAccount;
import com.kture.spring.service.exceptions.NotEnoughFundsOnBalance;
import com.kture.spring.service.exceptions.UserAccountNotFound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserAccountServiceImpl implements UserAccountService {

    private static final Logger logger = LoggerFactory.getLogger(UserAccountServiceImpl.class);

    @Autowired
    private UserAccountDAO userAccountDAO;

    @Override
    public UserAccount createAccount(UserAccount account) {
        UserAccount userAccount = userAccountDAO.createItem(account);
        logger.info("Create userAccount with userId %d and balance %d", userAccount.getUserId(), userAccount.getBalance());
        return userAccount;
    }

    @Override
    public Optional<UserAccount> getUserById(long id) {
        return userAccountDAO.getItemById(id);
    }

    @Override
    public boolean delete(long userId) {
        logger.info("Delete userAccount with userId %d", userId);
        return userAccountDAO.delete(userId);
    }

    @Override
    public void addToBalance(long userId, long sumToAdd) {
        Optional<UserAccount> account = getUserById(userId);
        if (!account.isPresent()) {
            throw new UserAccountNotFound();
        }

        logger.info("Adding %d to userAccount balance with userId %d", sumToAdd, userId);

        account.get().setBalance(account.get().getBalance() + sumToAdd);

        try {
            userAccountDAO.update(account.get());
        } catch (ItemNotFoundException e) {
            throw new UserAccountNotFound("", e);
        }
    }

    @Override
    public void spendFromBalance(long userId, long sumToSpend) throws NotEnoughFundsOnBalance {
        Optional<UserAccount> account = getUserById(userId);
        if (!account.isPresent()) {
            throw new UserAccountNotFound();
        }
        if (account.get().getBalance() < sumToSpend) {
            throw new NotEnoughFundsOnBalance();
        }

        logger.info("Spending %d from userAccount balance with userId %d", sumToSpend, userId);

        account.get().setBalance(account.get().getBalance() - sumToSpend);

        try {
            userAccountDAO.update(account.get());
        } catch (ItemNotFoundException e) {
            throw new UserAccountNotFound("", e);
        }
    }
}
