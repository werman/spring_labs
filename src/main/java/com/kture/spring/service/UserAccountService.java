package com.kture.spring.service;

import com.kture.spring.entity.UserAccount;
import com.kture.spring.service.exceptions.NotEnoughFundsOnBalance;

import java.util.Optional;

public interface UserAccountService {

    UserAccount createAccount(UserAccount account);

    Optional<UserAccount> getUserById(long id);

    boolean delete(long userId);

    void addToBalance(long userId, long sumToAdd);

    void spendFromBalance(long userId, long sumToSpend) throws NotEnoughFundsOnBalance;
}
