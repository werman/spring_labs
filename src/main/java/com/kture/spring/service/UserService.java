package com.kture.spring.service;

import com.kture.spring.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User createUser(User user);

    Optional<User> getUserById(long id);

    User updateUser(User user);

    boolean delete(long id);

    Optional<User> getByEmail(String email);

    List<User> getByName(String name);
}
