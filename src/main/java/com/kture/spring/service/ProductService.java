package com.kture.spring.service;

import com.kture.spring.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    Product createProduct(Product product);

    Optional<Product> getProductById(long id);

    Product updateProduct(Product product);

    boolean delete(long id);

    List<Product> getByUserId(long id);

    List<Product> getByTitle(String title);
}
