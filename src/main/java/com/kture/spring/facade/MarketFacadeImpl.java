package com.kture.spring.facade;

import com.kture.spring.entity.Product;
import com.kture.spring.entity.User;
import com.kture.spring.service.ProductService;
import com.kture.spring.service.UserAccountService;
import com.kture.spring.service.UserService;
import com.kture.spring.service.exceptions.NotEnoughFundsOnBalance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MarketFacadeImpl implements MarketFacade {

    private UserService userService;

    private ProductService productService;

    private UserAccountService userAccountService;

    @Override
    public User createUser(User user) {
        return userService.createUser(user);
    }

    @Override
    public User updateUser(User user) {
        return userService.updateUser(user);
    }

    @Override
    public User getUserById(long id) {
        return userService.getUserById(id).orElse(null);
    }

    @Override
    public User getUserByEmail(String email) {
        return userService.getByEmail(email).orElse(null);
    }

    @Override
    public List<User> getUsersByName(String name) {
        return userService.getByName(name);
    }

    @Override
    public boolean deleteUser(long id) {
        return userService.delete(id);
    }

    @Override
    public Product createProduct(Product product) {
        return productService.createProduct(product);
    }

    @Override
    public Product updateProduct(Product product) {
        return productService.createProduct(product);
    }

    @Override
    public Product getProductById(long id) {
        return productService.getProductById(id).orElse(null);
    }

    @Override
    public List<Product> getProductsByTitle(String title) {
        return productService.getByTitle(title);
    }

    @Override
    public List<Product> getProductsByUserId(long id) {
        return productService.getByUserId(id);
    }

    @Override
    public boolean deleteProduct(long id) {
        return productService.delete(id);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void buyProduct(Product product, User buyer) throws NotEnoughFundsOnBalance {
        userAccountService.spendFromBalance(buyer.getId(), product.getPrice());
        userAccountService.addToBalance(product.getUserId(), product.getPrice());
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setUserAccountService(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
}
