package com.kture.spring.controller;

import com.kture.spring.entity.User;
import com.kture.spring.facade.MarketFacade;
import com.kture.spring.service.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

@Controller("/")
public class MarketController {

    @Autowired
    MarketFacade marketFacade;

    @ExceptionHandler(ServiceException.class)
    public ModelAndView handle(Exception ex) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("error_msg", ex.getMessage());
        modelAndView.setViewName("error");
        return modelAndView;
    }

    @RequestMapping(value = "user/create", method = RequestMethod.POST)
    public String createUser(@RequestParam User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/error";
        }

        marketFacade.createUser(user);

        return "";
    }

    @RequestMapping(value = "user/update", method = RequestMethod.POST, consumes="application/json")
    public String updateUser(@RequestBody User user, BindingResult bindingResult) throws ServiceException {
        if (bindingResult.hasErrors()) {
            return "redirect:/error";
        }

        try {
            marketFacade.updateUser(user);
        } catch (UserNotFoundException ex) {
            throw new ServiceException("There is no user with such id!", ex);
        }

        return "";
    }

    @RequestMapping(value = "users/", params = "action=by_id", method = RequestMethod.GET)
    public String getUsers(Model model, @RequestParam Long id) {
        User user = marketFacade.getUserById(id);

        if (user != null) {
            model.addAttribute("users", Arrays.asList(user));
            return "/users";
        } else {
            return "redirect:/error";
        }
    }

    @RequestMapping(value = "users/", params = "action=by_name", method = RequestMethod.GET)
    public String getUsersByName(Model model, @RequestParam String name) {
        List<User> users = marketFacade.getUsersByName(name);
        model.addAttribute("users", users);
        return "/users";
    }

    @RequestMapping(value = "users/", params = "action=by_email", method = RequestMethod.GET)
    public String getUsersByEmail(Model model, @RequestParam String email) {
        User user = marketFacade.getUserByEmail(email);

        if (user != null) {
            model.addAttribute("users", Arrays.asList(user));
            return "/users";
        } else {
            return "redirect:/error";
        }
    }
}
